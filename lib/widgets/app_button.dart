import 'package:flutter/material.dart';

class AppButton extends StatelessWidget {
  double widthSize;
  double heightSize;
  double radius;
  Color color;
  final Widget child;
  AppButton(
      {Key? key,
      this.widthSize = 300,
      this.heightSize = 40,
      this.radius = 25,
      this.color=Colors.blue,
      required this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: widthSize,
      height: heightSize,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(radius),
        color: color,
      ),
      child: child,
    );
  }
}
