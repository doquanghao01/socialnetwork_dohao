import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:socialnetwork_hao/modle/user.dart';

class AppCircleAvatar extends StatelessWidget {
  int status;
  String img;
  double radiusRim;
  double radiusImg;
  AppCircleAvatar(
      {Key? key,
      required this.status,
      required this.img,
      this.radiusRim = 40,
      this.radiusImg = 30})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      backgroundColor:
          status == 1 ? Colors.red : const Color.fromARGB(255, 218, 218, 218),
      radius: radiusRim,
      child: CircleAvatar(
        radius: radiusImg,
        backgroundImage: NetworkImage(img),
      ),
    );
  }
}
