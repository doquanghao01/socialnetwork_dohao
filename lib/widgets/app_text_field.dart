import 'package:flutter/cupertino.dart';

class AppTextField extends StatelessWidget {
  double heightSize;
  double radius;
  Color color;
  final Widget child;
  AppTextField({
    Key? key,
    this.heightSize = 50,
    this.radius = 15,
    this.color = const Color.fromARGB(255, 236, 236, 236),
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size widthSize = MediaQuery.of(context).size;
    return Container(
      alignment: Alignment.center,
      margin: const EdgeInsets.symmetric(vertical: 10),
      padding: const EdgeInsets.only(left: 20),
      width: widthSize.width * 1,
      height: heightSize,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(radius),
      ),
      child: child,
    );
  }
}
