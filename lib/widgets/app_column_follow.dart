import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'app_large_text.dart';

class AppColumnFollow extends StatelessWidget {
  String amount;
  String text;
  Color color;
  AppColumnFollow(
      {Key? key,
      required this.amount,
      required this.text,
      this.color = Colors.black})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        children: [
          AppLargeText(
            text: amount,
            size: 25,
            color: color,
          ),
          AppLargeText(
            text: text,
            size: 20,
            color: color,
          )
        ],
      ),
    );
  }
}
