class User {
  int id;
  String name;
  String img;
  int statusOnOff;
  int statusLive;
  User(this.id, this.name, this.img, this.statusOnOff, this.statusLive);
}

class Comment {
  String name;
  String comment;
  Comment(this.name, this.comment);
}

class userList {
  List<Comment> listComment = [
    Comment('Nola Padilla', 'Humour, it use used middle.'),
    Comment('Kristie Smith', 'Sentence free chunks is in.')
  ];
  List<User> listUser = [
    User(
        1,
        'Huyền cute',
        'https://1.bp.blogspot.com/-bhApLXUh59Y/Xo8mwHtO9TI/AAAAAAAAb3o/rNfRFfoati8bOXyQJO8EUaSciPAW9xdHgCLcBGAsYHQ/s1600/Anh-gai-xinh-deo-kinh-2k%2B%25285%2529.jpg',
        1,
        1),
    User(
        2,
        'Nguyễn Ngọc Ánh',
        'https://4.bp.blogspot.com/-qNzhEXcSAjY/XJSH4PZZBuI/AAAAAAAArGA/eSeTQ6ieqp0rFJV0vFkMSJzzbwmvlkUHwCLcBGAs/s1600/53666494_2352017985084086_3639378295323099136_o.jpg',
        1,
        0),
    User(
        3,
        'Đỗ Thùy Dung',
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS1ilmTeNV-QDn18ASbzFlHhHrr9PFE3aUQrA&usqp=CAU',
        1,
        0),
    User(
        0,
        'Nguyễn Hoài An',
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSUeFRuXJAZSgOXPjDkwjQmY2HL1Bhm4gymuS-tLSoguI1f_rkhIZTmaBZspjA2jkbh_5s&usqp=CAU',
        0,
        0),
    User(
        1,
        'Nguyễn Gia Hân',
        'https://allimages.sgp1.digitaloceanspaces.com/photographercomvn/2021/05/1620302109_157_Top-99-hinh-anh-hot-girl-xinh-me-hon-quen.jpg',
        0,
        0),
    User(
        1,
        'Đỗ Thảo Linh',
        'https://kenh14cdn.com/203336854389633024/2021/5/27/thaonhile1900664993059306143693438777978355156364987n-162207545142142638700.jpg',
        0,
        0),
    User(
        1,
        'Lê Thanh Mai',
        'https://1.bp.blogspot.com/-bl32BAJfpTU/Xo8mtdirnOI/AAAAAAAAb3Y/R6oFS8ap4SoofEqeVgMBi1nu0I9iUQ_pgCLcBGAsYHQ/s1600/Anh-gai-xinh-deo-kinh-2k%2B%252846%2529.jpg',
        0,
        0),
    User(
        1,
        'Phùng Thị Tuyết Lan',
        'https://cdn.24h.com.vn/upload/2-2022/images/2022-05-16/Co-gai-noi-bat-o-bai-bien-Hawaii-nho-mac-ton-dac-diem-279266997_512316893924037_3784097687237503646_n-1652668656-488-width1000height1250.jpg',
        0,
        0)
  ];
}
