import 'package:flutter/material.dart';
import 'package:socialnetwork_hao/widgets/app_button.dart';
import 'package:socialnetwork_hao/widgets/app_large_text.dart';
import 'package:socialnetwork_hao/widgets/app_text.dart';
import 'package:socialnetwork_hao/widgets/app_text_field.dart';

import '../navpages/main_page.dart';
import 'register.dart';

class Singin extends StatefulWidget {
  const Singin({Key? key}) : super(key: key);

  @override
  State<Singin> createState() => _SinginState();
}

class _SinginState extends State<Singin> {
  bool showText = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        padding: const EdgeInsets.only(top: 60, left: 20, right: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            AppLargeText(text: "Welecome Back!"),
            const SizedBox(
              height: 20,
            ),
            AppTextField(
              heightSize: 55,
              child: const TextField(
                decoration: InputDecoration(
                  icon: Icon(
                    Icons.email_outlined,
                    color: Color.fromARGB(255, 126, 126, 126),
                  ),
                  hintText: "Email address",
                  border: InputBorder.none,
                ),
              ),
            ),
            AppTextField(
              heightSize: 55,
              child: TextField(
                obscureText: showText,
                decoration: InputDecoration(
                  icon: const Icon(
                    Icons.lock_outline,
                    color: Color.fromARGB(255, 126, 126, 126),
                  ),
                  hintText: "Password",
                  border: InputBorder.none,
                  suffixIcon: IconButton(
                    onPressed: () {
                      setState(() {
                        showText = !showText;
                      });
                    },
                    icon: showText
                        ? const Icon(Icons.visibility_off_outlined)
                        : const Icon(Icons.visibility),
                    color: const Color.fromARGB(255, 126, 126, 126),
                  ),
                ),
              ),
            ),
            Container(
              alignment: Alignment.centerRight,
              width: double.infinity,
              child: TextButton(
                onPressed: () {},
                child: AppText(
                  text: "Forgot Password ?",
                  size: 18,
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            InkWell(
              onTap: () => Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const MainPage(),
                ),
              ),
              child: AppButton(
                widthSize: MediaQuery.of(context).size.width * 1,
                heightSize: 40,
                child: AppText(
                  text: "Sign In",
                  color: Colors.white,
                  size: 20,
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.only(top: 20),
              width: double.maxFinite,
              alignment: Alignment.center,
              child: InkWell(
                onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const Register(),
                  ),
                ),
                child: Text(
                  "I haven't an account",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontSize: 18,
                      color: Colors.black.withOpacity(0.6)),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
