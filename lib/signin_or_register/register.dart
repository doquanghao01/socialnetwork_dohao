import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:socialnetwork_hao/signin_or_register/singin.dart';

import '../widgets/app_button.dart';
import '../widgets/app_large_text.dart';
import '../widgets/app_text.dart';
import '../widgets/app_text_field.dart';

class Register extends StatefulWidget {
  const Register({Key? key}) : super(key: key);

  @override
  State<Register> createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  bool showText = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        padding: const EdgeInsets.only(top: 60, left: 20, right: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            AppLargeText(text: "Create an Account"),
            const SizedBox(
              height: 20,
            ),
            AppTextField(
              heightSize: 55,
              child: const TextField(
                decoration: InputDecoration(
                  icon: Icon(
                    Icons.person,
                    color: Color.fromARGB(255, 126, 126, 126),
                  ),
                  hintText: "Username",
                  border: InputBorder.none,
                ),
              ),
            ),
            AppTextField(
              heightSize: 55,
              child: const TextField(
                decoration: InputDecoration(
                  icon: Icon(
                    Icons.email_outlined,
                    color: Color.fromARGB(255, 126, 126, 126),
                  ),
                  hintText: "Email address",
                  border: InputBorder.none,
                ),
              ),
            ),
            AppTextField(
              heightSize: 55,
              child: TextField(
                obscureText: showText,
                decoration: InputDecoration(
                  icon: const Icon(
                    Icons.email_outlined,
                    color: Color.fromARGB(255, 126, 126, 126),
                  ),
                  hintText: "Password",
                  border: InputBorder.none,
                  suffixIcon: IconButton(
                    onPressed: () {
                      setState(() {
                        showText = !showText;
                      });
                    },
                    icon: showText
                        ? const Icon(Icons.visibility_off_outlined)
                        : const Icon(Icons.visibility),
                    color: const Color.fromARGB(255, 126, 126, 126),
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            AppButton(
              widthSize: MediaQuery.of(context).size.width * 1,
              heightSize: 40,
              child: AppText(
                text: "Register",
                color: Colors.white,
                size: 20,
              ),
            ),
            Container(
              padding: const EdgeInsets.only(top: 20),
              width: double.maxFinite,
              alignment: Alignment.center,
              child: InkWell(
                onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const Singin(),
                  ),
                ),
                child: Text(
                  "I've alreadt an account",
                  style: TextStyle(
                    decoration: TextDecoration.underline,
                    fontSize: 18,
                    color: Colors.black.withOpacity(0.6),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
