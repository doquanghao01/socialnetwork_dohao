import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:socialnetwork_hao/modle/user.dart';
import 'package:socialnetwork_hao/widgets/app_large_text.dart';
import 'package:socialnetwork_hao/widgets/app_text.dart';

import '../widgets/app_button.dart';
import '../widgets/app_column_follow.dart';
import 'account_information.dart';

class PersonalFollowPage extends StatelessWidget {
  PersonalFollowPage({Key? key, required this.user}) : super(key: key);
  User user;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        decoration: BoxDecoration(
          image:
              DecorationImage(image: NetworkImage(user.img), fit: BoxFit.cover),
        ),
        child: Container(
          padding: const EdgeInsets.only(top: 50),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: Container(
                      alignment: Alignment.topLeft,
                      child: const Icon(
                        Icons.chevron_left,
                        size: 30,
                        color: Colors.white,
                      )),
                ),
              ),
              Contrainer(
                user: user,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class Contrainer extends StatelessWidget {
  Contrainer({Key? key, required this.user}) : super(key: key);
  User user;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
          left: 20,
          right: 20,
          top: MediaQuery.of(context).size.height / 2,
          bottom: 40),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: FractionalOffset.bottomCenter,
          end: FractionalOffset.topCenter,
          colors: [
            Colors.black.withOpacity(0.7),
            Colors.grey.withOpacity(0.3),
            Colors.grey.withOpacity(0.0),
          ],
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          AppLargeText(text: 'Zayn Molloy', size: 40, color: Colors.white),
          AppText(text: 'Fashion Model', size: 20, color: Colors.white),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              AppColumnFollow(
                amount: '15',
                text: 'Posts',
                color: Colors.white,
              ),
              AppColumnFollow(
                amount: '486',
                text: 'Fllowers',
                color: Colors.white,
              ),
              AppColumnFollow(
                amount: '58',
                text: 'Following',
                color: Colors.white,
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(top: 15),
                  child: AppButton(
                    widthSize: MediaQuery.of(context).size.width * 1,
                    heightSize: 40,
                    child: AppText(
                      text: "Follow",
                      color: Colors.white,
                      size: 20,
                    ),
                  ),
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 12),
                child: InkWell(
                  onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Accpunt_information(
                        user: user,
                      ),
                    ),
                  ),
                  child: Container(
                    width: 35,
                    height: 35,
                    decoration: BoxDecoration(
                      color: const Color.fromARGB(255, 175, 175, 175),
                      borderRadius: BorderRadius.circular(25),
                    ),
                    child: const Icon(
                      Icons.trending_flat_outlined,
                      size: 25,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
