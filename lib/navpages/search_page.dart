import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:socialnetwork_hao/modle/user.dart';
import 'package:socialnetwork_hao/widgets/app_button.dart';
import 'package:socialnetwork_hao/widgets/app_circleavatar.dart';
import 'package:socialnetwork_hao/widgets/app_large_text.dart';
import 'package:socialnetwork_hao/widgets/app_text.dart';
import 'package:socialnetwork_hao/widgets/app_text_field.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  @override
  Widget build(BuildContext context) {
    userList list = userList();
    return Container(
      padding: const EdgeInsets.only(left: 20, right: 20, top: 40),
      child: Column(children: [
        AppTextField(
          heightSize: 43,
          child: const TextField(
            decoration: InputDecoration(
              icon: Icon(
                Icons.search_outlined,
                color: Color.fromARGB(255, 126, 126, 126),
              ),
              hintText: "Search messages",
              border: InputBorder.none,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              AppLargeText(text: 'Recent'),
              AppText(
                text: 'Clear',
                color: Colors.blue,
              )
            ],
          ),
        ),
        Recent(list: list),
        Container(
          padding: const EdgeInsets.only(top: 20),
          alignment: Alignment.centerLeft,
          child: AppLargeText(text: 'Result'),
        ),
        Result(list: list),
      ]),
    );
  }
}

class Result extends StatelessWidget {
  const Result({
    Key? key,
    required this.list,
  }) : super(key: key);

  final userList list;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: List.generate(
            list.listUser.length,
            (index) => Padding(
              padding: const EdgeInsets.only(top: 15),
              child: Row(
                children: [
                  Status(user: list.listUser[index]),
                  const SizedBox(
                    height: 10,
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          AppLargeText(
                            text: list.listUser[index].name,
                            size: 20,
                          ),
                          AppText(text: "@${list.listUser[index].id}"),
                        ],
                      ),
                    ),
                  ),
                  AppButton(
                    widthSize: 80,
                    heightSize: 35,
                    radius: 10,
                    child: AppText(
                      text: "Follow",
                      color: Colors.white,
                      size: 15,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class Recent extends StatelessWidget {
  const Recent({
    Key? key,
    required this.list,
  }) : super(key: key);

  final userList list;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: List.generate(
          list.listUser.length,
          (index) => Padding(
            padding: const EdgeInsets.only(right: 10, top: 10),
            child: Column(
              children: [
                Status(user: list.listUser[index]),
                const SizedBox(
                  height: 10,
                ),
                AppText(text: list.listUser[index].name),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class Status extends StatelessWidget {
  const Status({
    Key? key,
    required this.user,
  }) : super(key: key);

  final User user;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 65,
      height: 65,
      child: Stack(
        children: [
          AppCircleAvatar(
            status: 0,
            img: user.img,
            radiusImg: 35,
            radiusRim: 40,
          ),
          Visibility(
            visible: user.statusOnOff == 1 ? true : false,
            child: Positioned(
              top: 50,
              left: 47,
              child: Container(
                width: 13,
                height: 13,
                decoration: const BoxDecoration(
                    color: Colors.green, shape: BoxShape.circle),
              ),
            ),
          )
        ],
      ),
    );
  }
}
