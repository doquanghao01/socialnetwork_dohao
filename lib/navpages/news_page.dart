import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:socialnetwork_hao/modle/user.dart';

import '../widgets/app_text.dart';
import '../widgets/app_text_field.dart';

class News_page extends StatelessWidget {
  News_page({Key? key, required this.user}) : super(key: key);
  User user;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.only(top: 50, left: 10, right: 10),
        child: Column(
          children: [
            const LinearProgressIndicator(
              color: Colors.black45,
              backgroundColor: Colors.black12,
              value: 0.2,
            ),
            Information(user: user),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Image.network(
                  user.img,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Row(
              children: [
                Expanded(
                  child: AppTextField(
                    heightSize: 43,
                    child: const TextField(
                      decoration: InputDecoration(
                        hintText: "Send message",
                        border: InputBorder.none,
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
                const Icon(Icons.send),
              ],
            )
          ],
        ),
      ),
    );
  }
}

class Information extends StatelessWidget {
  const Information({
    Key? key,
    required this.user,
  }) : super(key: key);

  final User user;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20),
            child: CircleAvatar(
              radius: 20,
              backgroundImage: NetworkImage(user.img),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: user.name,
                          style: const TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                        const WidgetSpan(
                          child: Padding(
                            padding: EdgeInsets.only(left: 10),
                          ),
                        ),
                        const TextSpan(
                          text: '10 min',
                          style: TextStyle(fontSize: 15),
                        ),
                      ],
                    ),
                  ),
                  const Text('106 views'),
                ],
              ),
            ),
          ),
          const Padding(
            padding: EdgeInsets.only(right: 20),
            child: Icon(
              Icons.more_vert,
              color: Color.fromARGB(255, 105, 105, 105),
            ),
          ),
        ],
      ),
    );
  }
}
