import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:socialnetwork_hao/modle/user.dart';
import 'package:socialnetwork_hao/widgets/app_large_text.dart';

import '../widgets/app_button.dart';
import '../widgets/app_column_follow.dart';
import '../widgets/app_text.dart';

class Accpunt_information extends StatelessWidget {
  Accpunt_information({Key? key, required this.user}) : super(key: key);
  User user;
  var img = [
    'https://topshare.vn/wp-content/uploads/2021/10/hinh-anh-nen-anime-phong-canh-dep-35.jpg',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQZzkUrCSVkfBKZfOPI1OLoMbOEdhy-aFKh0sDBAAXvjN8IyPvCJi4nsEk4JM3vpJGWWfo&usqp=CAU',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQtV9h2YCS2g8t-P3Ra1Mst1VJ23arPJx8YXiVcQY-be6NUVVxlRNjm-knO1qY-35elebk&usqp=CAU',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSyBRNxcmlwBqVQyRJ1AaVRgidtDs5jfeVELkBxF6aBmLps8WHzDIR5bIaP7ovL-_QvtTc&usqp=CAU',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQMmJbZL_t2hbWcoczkMXGNBuXLqvnsFMhtgQ&usqp=CAU',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4SNJ1E1eh2ukdHFCUckPHVUH6u9KwkqpEwojJakoqKpi_a9FLk545f9S2Uh1AFMdAuCc&usqp=CAU',
    'https://i.pinimg.com/474x/b1/d6/cf/b1d6cff4b2c77fbd200b97874329e72d.jpg',
    'https://img5.thuthuatphanmem.vn/uploads/2021/11/26/anh-phong-canh-co-trang-trung-quoc-buon-tuyet-dep_010556327.jpg'
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          padding: const EdgeInsets.only(top: 50, left: 20, right: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Iconback(),
              Information(user: user),
              const Connect(),
              const SizedBox(
                height: 20,
              ),
              AppLargeText(text: 'Posts'),
              const SizedBox(
                height: 30,
              ),
              Posts(img: img)
            ],
          )),
    );
  }
}

class Posts extends StatelessWidget {
  const Posts({
    Key? key,
    required this.img,
  }) : super(key: key);

  final List<String> img;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: GridView.builder(
          padding: EdgeInsets.zero,
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
          ),
          itemCount: img.length,
          itemBuilder: (BuildContext context, int index) {
            return Padding(
              padding: const EdgeInsets.all(5.0),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  image: DecorationImage(
                    image: NetworkImage(img[index]),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            );
          }),
    );
  }
}

class Connect extends StatelessWidget {
  const Connect({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: AppButton(
              widthSize: MediaQuery.of(context).size.width * 1,
              heightSize: 40,
              child: AppText(
                text: "Connect",
                color: Colors.white,
                size: 20,
              ),
            ),
          ),
          AppColumnFollow(amount: '105k', text: 'Fllowers'),
          AppColumnFollow(amount: '45', text: 'Following'),
        ],
      ),
    );
  }
}

class Information extends StatelessWidget {
  const Information({
    Key? key,
    required this.user,
  }) : super(key: key);

  final User user;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        CircleAvatar(
          radius: 35,
          backgroundImage: NetworkImage(user.img),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(left: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AppLargeText(text: user.name),
                AppText(
                  text: 'April 12th',
                  size: 20,
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class Iconback extends StatelessWidget {
  const Iconback({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 15),
      child: Container(
          alignment: Alignment.topLeft,
          child: const Icon(
            Icons.chevron_left,
            size: 30,
            color: Colors.black,
          )),
    );
  }
}
