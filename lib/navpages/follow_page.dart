import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:socialnetwork_hao/modle/user.dart';
import 'package:socialnetwork_hao/widgets/app_circleavatar.dart';
import 'package:socialnetwork_hao/widgets/app_large_text.dart';
import 'package:socialnetwork_hao/widgets/app_text.dart';

import '../widgets/app_button.dart';

class FollowPage extends StatefulWidget {
  const FollowPage({Key? key}) : super(key: key);

  @override
  State<FollowPage> createState() => _FollowPageState();
}

class _FollowPageState extends State<FollowPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 20, right: 20, top: 50),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const FollowRequests(),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: AppLargeText(
              text: 'Today',
            ),
          ),
          const Today(),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: AppLargeText(
              text: 'Recent',
            ),
          ),
          const Recent(),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: AppLargeText(
              text: 'Suggestions',
            ),
          ),
          const Suggestions(),
        ],
      ),
    );
  }
}

class Suggestions extends StatelessWidget {
  const Suggestions({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    userList list = userList();
    return Expanded(
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: List.generate(
            list.listUser.length,
            (index) => Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Row(
                children: [
                  SizedBox(
                    width: 65,
                    height: 65,
                    child: AppCircleAvatar(
                      status: 0,
                      img: list.listUser[index].img,
                      radiusImg: 30,
                      radiusRim: 40,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          AppLargeText(
                            text: list.listUser[index].name,
                            size: 20,
                          ),
                          AppText(text: 'Matt Hayden')
                        ],
                      ),
                    ),
                  ),
                  AppButton(
                    widthSize: 80,
                    heightSize: 35,
                    radius: 10,
                    child: AppText(
                      text: "Follow",
                      color: Colors.white,
                      size: 15,
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Icon(
                      Icons.clear,
                      size: 15,
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class Recent extends StatelessWidget {
  const Recent({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: EdgeInsets.zero,
      itemCount: 2,
      shrinkWrap: true,
      itemBuilder: (context, index) => Padding(
        padding: const EdgeInsets.only(top: 10, left: 10),
        child: Row(
          children: [
            SizedBox(
              width: 65,
              height: 65,
              child: AppCircleAvatar(
                status: 0,
                img:
                    "https://i.pinimg.com/564x/82/0e/9f/820e9fc6b65a11d6679af65d9ee62654.jpg",
                radiusImg: 30,
                radiusRim: 40,
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Text.rich(
                  TextSpan(
                    children: [
                      TextSpan(
                        text: 'Follow',
                        style: TextStyle(
                          fontSize: 20,
                          color: Colors.black.withOpacity(0.4),
                        ),
                      ),
                      TextSpan(
                        text: ' Leonardo Carty, Reilly Sadler',
                        style: TextStyleSpanBold(),
                      ),
                      TextSpan(
                        text:
                            ' and others you know to see their photos and videos. 5w',
                        style: TextStyleSpan(),
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  TextStyle TextStyleSpanBold() {
    return TextStyle(
      fontSize: 20,
      fontWeight: FontWeight.bold,
      color: Colors.black.withOpacity(0.6),
    );
  }

  TextStyle TextStyleSpan() {
    return TextStyle(
      fontSize: 20,
      color: Colors.black.withOpacity(0.4),
    );
  }
}

class Today extends StatelessWidget {
  const Today({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: Row(
        children: [
          Stack(
            children: [
              Container(
                padding: const EdgeInsets.only(left: 10),
                child: AppCircleAvatar(
                  img:
                      "https://i.pinimg.com/564x/82/0e/9f/820e9fc6b65a11d6679af65d9ee62654.jpg",
                  status: 0,
                  radiusRim: 30,
                  radiusImg: 27,
                ),
              ),
              Container(
                padding: const EdgeInsets.only(left: 25, top: 10),
                child: AppCircleAvatar(
                  img:
                      "https://i.pinimg.com/564x/82/0e/9f/820e9fc6b65a11d6679af65d9ee62654.jpg",
                  status: 0,
                  radiusRim: 30,
                  radiusImg: 27,
                ),
              ),
            ],
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Text.rich(
                TextSpan(
                  children: [
                    TextSpan(
                      text: 'amani.buchanan, layton_beck',
                      style: TextStyleSpanBold(),
                    ),
                    TextSpan(
                      text: ' and',
                      style: TextStyleSpan(),
                    ),
                    TextSpan(
                      text: ' 5 others',
                      style: TextStyleSpanBold(),
                    ),
                    TextSpan(
                      text: ' started following you. 2d',
                      style: TextStyleSpan(),
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  TextStyle TextStyleSpanBold() {
    return TextStyle(
      fontSize: 20,
      fontWeight: FontWeight.bold,
      color: Colors.black.withOpacity(0.6),
    );
  }

  TextStyle TextStyleSpan() {
    return TextStyle(
      fontSize: 20,
      color: Colors.black.withOpacity(0.4),
    );
  }
}

class FollowRequests extends StatelessWidget {
  const FollowRequests({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        CircleAvatar(
          backgroundColor: Colors.black.withOpacity(0.4),
          radius: 27,
          child: CircleAvatar(
            radius: 25,
            backgroundColor: Colors.white,
            child: Icon(
              Icons.person_add_outlined,
              size: 28,
              color: Colors.black.withOpacity(0.4),
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(left: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AppLargeText(text: 'Follow Requests'),
                AppText(text: 'Approve or ignore request')
              ],
            ),
          ),
        )
      ],
    );
  }
}
