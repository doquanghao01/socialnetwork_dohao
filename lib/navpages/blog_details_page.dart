import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:socialnetwork_hao/modle/user.dart';
import '../widgets/app_circleavatar.dart';
import '../widgets/app_large_text.dart';
import '../widgets/app_text.dart';
import '../widgets/app_text_field.dart';

class BlogDetailsPage extends StatefulWidget {
  BlogDetailsPage({Key? key, required this.user}) : super(key: key);
  User user;
  @override
  State<BlogDetailsPage> createState() => _BlogDetailsPageState();
}

class _BlogDetailsPageState extends State<BlogDetailsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Container(
          padding: const EdgeInsets.only(top: 50, left: 10, right: 10),
          child: Column(
            children: [
              Information(widget: widget),
              Padding(
                padding: const EdgeInsets.only(top: 15, bottom: 10),
                child: Image(
                  image: NetworkImage(widget.user.img),
                  width: double.maxFinite,
                  fit: BoxFit.fitWidth,
                ),
              ),
              const favourite(),
              const Posts(),
            ],
          ),
        ),
      ),
    );
  }
}

class Posts extends StatelessWidget {
  const Posts({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          AppText(
            text:
                '- Free is Ipsum 15th the variations a alteration attributed with 15th There even by there century graphic.',
            color: Colors.black,
            size: 18,
          ),
          const Text.rich(
            TextSpan(
              children: [
                TextSpan(
                  text: 'Have of designs, true ',
                  style: TextStyle(fontSize: 18),
                ),
                WidgetSpan(child: Icon(Icons.tag_faces)),
                TextSpan(
                  text: ' are Ipsum De out ',
                  style: TextStyle(fontSize: 18),
                ),
                WidgetSpan(
                  child: Icon(Icons.tag_faces),
                  style: TextStyle(fontSize: 18),
                ),
                TextSpan(
                  text: ' are ',
                  style: TextStyle(fontSize: 18),
                ),
                WidgetSpan(child: Icon(Icons.tag_faces)),
                TextSpan(
                  text:
                      ' Internet predefined alteration bonorum free specimen.',
                  style: TextStyle(fontSize: 18),
                ),
              ],
            ),
          ),
          Padding(
              padding: const EdgeInsets.only(top: 10),
              child: AppText(
                text: 'View all 28 comments',
                size: 18,
              )),
          Row(
            children: [
              Expanded(
                child: AppTextField(
                  heightSize: 43,
                  child: const TextField(
                    decoration: InputDecoration(
                      hintText: "Comment me",
                      border: InputBorder.none,
                    ),
                  ),
                ),
              ),
              Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: AppText(
                    text: 'Post',
                    color: Colors.blue,
                    size: 20,
                  ))
            ],
          )
        ],
      ),
    );
  }
}

class favourite extends StatelessWidget {
  const favourite({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Row(
        children: [
          const Icon(Icons.favorite_outline),
          const Padding(
            padding: EdgeInsets.only(left: 10),
            child: Icon(Icons.chat_bubble_outline),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: SizedBox(
                width: 23,
                height: 23,
                child: SvgPicture.asset(
                    'assets/icons/forward-send-svgrepo-com.svg')),
          ),
          Expanded(
            child: Container(
              alignment: Alignment.bottomRight,
              child: AppText(text: '7,327 views'),
            ),
          )
        ],
      ),
    );
  }
}

class Information extends StatelessWidget {
  const Information({
    Key? key,
    required this.widget,
  }) : super(key: key);

  final BlogDetailsPage widget;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20),
            child: AppCircleAvatar(
              img: widget.user.img,
              status: 0,
              radiusImg: 20,
              radiusRim: 22,
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 8),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AppLargeText(
                    text: widget.user.name,
                    size: 20,
                  ),
                  AppText(text: 'Surat, Gujarat')
                ],
              ),
            ),
          ),
          const Padding(
            padding: EdgeInsets.only(right: 20),
            child: Text("12 min"),
          ),
        ],
      ),
    );
  }
}
