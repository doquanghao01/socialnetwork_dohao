import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:socialnetwork_hao/modle/user.dart';
import 'package:socialnetwork_hao/widgets/app_large_text.dart';
import 'package:socialnetwork_hao/widgets/app_text.dart';

import '../widgets/app_circleavatar.dart';
import 'blog_details_page.dart';
import 'news_page.dart';
import 'personal_follow_page.dart';

class MyPage extends StatefulWidget {
  const MyPage({Key? key}) : super(key: key);

  @override
  State<MyPage> createState() => _MyPageState();
}

class _MyPageState extends State<MyPage> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: const [
        UseStatus(),
        ListBlog(),
      ],
    );
  }
}

class ListBlog extends StatelessWidget {
  const ListBlog({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    userList list = userList();
    return Expanded(
      child: ListView.builder(
        padding: EdgeInsets.zero,
        itemCount: list.listUser.length,
        itemBuilder: (context, index) => Container(
          padding: const EdgeInsets.only(left: 10, right: 10, bottom: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: double.maxFinite,
                height: 0.5,
                color: const Color.fromARGB(255, 196, 196, 196),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 8),
                      child: InkWell(
                        onTap: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>
                                PersonalFollowPage(user: list.listUser[index]),
                          ),
                        ),
                        child: AppCircleAvatar(
                          img: list.listUser[index].img,
                          status: 0,
                          radiusImg: 20,
                          radiusRim: 22,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 8),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            AppLargeText(
                              text: list.listUser[index].name,
                              size: 20,
                            ),
                            AppText(text: 'Surat, Gujarat')
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 20),
                      child: AppText(text: "12 min"),
                    ),
                  ],
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) =>
                          BlogDetailsPage(user: list.listUser[index]),
                    ),
                  );
                },
                child: Padding(
                  padding: const EdgeInsets.only(top: 15, bottom: 10),
                  child: Image(
                    image: NetworkImage(list.listUser[index].img),
                    width: double.maxFinite,
                    fit: BoxFit.fitWidth,
                  ),
                ),
              ),
              Row(
                children: [
                  StackImg(list: list),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: AppText(text: '21 Like this'),
                    ),
                  ),
                  const Icon(
                    Icons.more_vert,
                    color: Colors.black54,
                  ),
                ],
              ),
              const ListComment(),
            ],
          ),
        ),
      ),
    );
  }
}

class StackImg extends StatelessWidget {
  const StackImg({
    Key? key,
    required this.list,
  }) : super(key: key);

  final userList list;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          padding: const EdgeInsets.only(left: 10),
          child: AppCircleAvatar(
            status: 0,
            img: list.listUser[0].img,
            radiusImg: 13,
            radiusRim: 15,
          ),
        ),
        Container(
          padding: const EdgeInsets.only(left: 30),
          child: AppCircleAvatar(
            status: 0,
            img: list.listUser[1].img,
            radiusImg: 13,
            radiusRim: 15,
          ),
        ),
        Container(
          padding: const EdgeInsets.only(left: 50),
          child: AppCircleAvatar(
            status: 0,
            img: list.listUser[2].img,
            radiusImg: 13,
            radiusRim: 15,
          ),
        ),
      ],
    );
  }
}

class ListComment extends StatelessWidget {
  const ListComment({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    userList list = userList();
    return Container(
      margin: const EdgeInsets.only(top: 10, left: 8),
      width: double.maxFinite,
      height: 50,
      child: ListView.builder(
        padding: EdgeInsets.zero,
        itemCount: 2,
        shrinkWrap: true,
        itemBuilder: (context, index) => Padding(
          padding: const EdgeInsets.only(top: 8),
          child: Row(
            children: [
              AppLargeText(
                text: list.listComment[index].name,
                size: 18,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 5),
                child: AppText(
                  text: list.listComment[index].comment,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class UseStatus extends StatelessWidget {
  const UseStatus({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    userList userlist = userList();
    return Container(
      height: 150,
      padding: const EdgeInsets.only(top: 50, left: 18, right: 18),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: [
            Container(
              width: 70,
              height: 70,
              padding: const EdgeInsets.only(right: 8),
              child: Stack(children: [
                AppCircleAvatar(
                  img:
                      "https://i.pinimg.com/564x/82/0e/9f/820e9fc6b65a11d6679af65d9ee62654.jpg",
                  status: 0,
                  radiusImg: 30,
                ),
                Positioned(
                  top: 43,
                  left: 36,
                  child: Container(
                    width: 20,
                    height: 20,
                    decoration: const BoxDecoration(
                        color: Colors.blue, shape: BoxShape.circle),
                    child: const Icon(Icons.add, size: 21, color: Colors.white),
                  ),
                )
              ]),
            ),
            Row(
              children: List.generate(
                userlist.listUser.length,
                (index) => Container(
                  margin: const EdgeInsets.all(8),
                  width: 65,
                  height: 65,
                  child: Stack(children: [
                    InkWell(
                      onTap: () => userlist.listUser[index].statusOnOff == 1
                          ? Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => News_page(
                                  user: userlist.listUser[index],
                                ),
                              ),
                            )
                          : () => context,
                      child: AppCircleAvatar(
                          status: userlist.listUser[index].statusOnOff,
                          img: userlist.listUser[index].img),
                    ),
                    Visibility(
                      visible: userlist.listUser[index].statusLive == 1
                          ? true
                          : false,
                      child: Positioned(
                        left: 35,
                        child: Container(
                          width: 30,
                          height: 16,
                          decoration: BoxDecoration(
                            color: Colors.red,
                            borderRadius: BorderRadius.circular(8),
                          ),
                          child: const Center(
                            child: Text(
                              'Live',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 13),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ]),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
