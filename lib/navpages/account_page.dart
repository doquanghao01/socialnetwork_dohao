import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:socialnetwork_hao/widgets/app_large_text.dart';
import 'package:socialnetwork_hao/widgets/app_text.dart';

import '../widgets/app_button.dart';
import '../widgets/app_column_follow.dart';

class AccountPage extends StatefulWidget {
  const AccountPage({Key? key}) : super(key: key);

  @override
  State<AccountPage> createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 20, right: 20, top: 50),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const CircleAvatar(
            radius: 50,
            backgroundImage: NetworkImage(
                "https://i.pinimg.com/564x/82/0e/9f/820e9fc6b65a11d6679af65d9ee62654.jpg"),
          ),
          const Information(),
          const CardFollow(),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: AppLargeText(text: 'Complete your profile'),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 5),
            child: Text.rich(
              TextSpan(
                children: [
                  TextSpan(
                    text: '2 OF 4',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.green.withOpacity(0.8),
                    ),
                  ),
                  TextSpan(
                    text: ' COMPLELE',
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.black.withOpacity(0.6),
                    ),
                  ),
                ],
              ),
            ),
          ),
          const CompleteYourProfile(),
        ],
      ),
    );
  }
}

class CompleteYourProfile extends StatelessWidget {
  const CompleteYourProfile({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 15, right: 15),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: List.generate(
            4,
            (index) => Padding(
              padding: const EdgeInsets.only(right: 30),
              child: Card(
                color: const Color.fromARGB(255, 245, 245, 245),
                margin: const EdgeInsets.only(top: 20),
                shape: RoundedRectangleBorder(
                  side: BorderSide(
                      color: const Color.fromARGB(255, 223, 223, 223)
                          .withOpacity(0.3),
                      width: 2),
                  borderRadius: const BorderRadius.all(
                    Radius.circular(5),
                  ),
                ),
                child: Container(
                  width: 280,
                  padding: const EdgeInsets.only(
                      top: 30, bottom: 30, left: 30, right: 30),
                  child: Column(
                    children: <Widget>[
                      CircleAvatar(
                        backgroundColor: Colors.black.withOpacity(0.4),
                        radius: 30,
                        child: CircleAvatar(
                          radius: 27,
                          backgroundColor:
                              const Color.fromARGB(255, 241, 240, 240),
                          child: Icon(
                            Icons.person_outline,
                            size: 35,
                            color: Colors.black.withOpacity(0.4),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 10),
                        child: AppLargeText(
                          text: 'Add Profile Photo',
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(top: 10),
                        child: const Text(
                          "15th available always a first text looks for",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.black),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 10),
                        child: AppButton(
                          widthSize: 120,
                          heightSize: 40,
                          radius: 10,
                          child: AppText(
                            text: "Add Photo",
                            color: Colors.white,
                            size: 20,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class CardFollow extends StatelessWidget {
  const CardFollow({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.only(top: 20),
      shape: RoundedRectangleBorder(
        side: BorderSide(
            color: const Color.fromARGB(255, 223, 223, 223).withOpacity(0.5),
            width: 2),
        borderRadius: const BorderRadius.all(
          Radius.circular(5),
        ),
      ),
      child: Container(
        width: double.maxFinite,
        padding:
            const EdgeInsets.only(top: 40, bottom: 40, left: 10, right: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            AppColumnFollow(amount: '15', text: 'Posts'),
            AppColumnFollow(amount: '486', text: 'Fllowers'),
            AppColumnFollow(amount: '58', text: 'Following'),
          ],
        ),
      ),
    );
  }
}

class Information extends StatelessWidget {
  const Information({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 10,
              ),
              AppLargeText(text: 'Martyn Rankin'),
              AppText(
                text: 'Fashion Model',
                size: 20,
              ),
              AppText(
                text: 'I love to be kind',
                size: 20,
              ),
              AppText(
                text: 'From heaven',
                size: 20,
              ),
              const Text(
                "See more",
                style: TextStyle(
                  fontSize: 20,
                  decoration: TextDecoration.underline,
                  color: Colors.black54,
                ),
              ),
            ],
          ),
        ),
        AppButton(
          widthSize: 80,
          heightSize: 35,
          radius: 10,
          child: AppText(
            text: "Edit",
            color: Colors.white,
            size: 15,
          ),
        ),
      ],
    );
  }
}
